# srhinow/tinymce-plugins

Das ist eine Contao-Erweiterung vom Type contao-module, welches zusätzlich zu den vom contao-core bereitgestellten TinyMCE-Plugins, weitere Open Source Plugins in assetes/tinymce4 verlinkt.

Eine Dokumetation zu den Plugins findet man hier: https://www.tiny.cloud/docs/ [TinyMCE v5] im Bereich Plugins / Open Source plugins

**Folgende Plugins werden bereitgestellt:**

advlist, anchor, autolink, autoresize, bbcode, codemirror, colorpicker, contextmenu, emoticons, fullpage, help, hr, imagetools,  insertdatetime, legacyoutput, media, nonbreaking, noneditable, pagebreak,  preview, print, save, spellchecker,  textcolor, textpattern, toc, wordcount

