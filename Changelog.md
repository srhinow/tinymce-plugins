# Changes in srhinow/tinymce-plugins
# 2.0.1 (31.10.2022)
- remove public: true in services.yaml

# 2.0.0 (31.10.2022)
- ready symlink per type: contao-bundle and EventListener-Class

## 1.0.9 (26.10.2022)
- ready symlink per type: contao-module and contao-source entries in composer.json