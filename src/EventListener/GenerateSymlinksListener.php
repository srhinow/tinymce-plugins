<?php
/*
 * This file is part of the Contao extension tinymce-plugins.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

namespace Srhinow\TinymcePlugins\EventListener;

use Contao\CoreBundle\Event\ContaoCoreEvents;
use Contao\CoreBundle\Event\GenerateSymlinksEvent;
use Terminal42\ServiceAnnotationBundle\Annotation\ServiceTag;

/**
 * @ServiceTag("kernel.event_listener", event=ContaoCoreEvents::GENERATE_SYMLINKS)
 */
class GenerateSymlinksListener
{
    public function __invoke(GenerateSymlinksEvent $event): void
    {
        $srcPath = 'vendor/srhinow/tinymce-plugins/assets/tinymce4/js/plugins/';
        $linkPath = 'assets/tinymce4/js/plugins/';

        $arrPlugins = ['advlist','anchor','autolink','autoresize','bbcode','codemirror','colorpicker','contextmenu','emoticons','fullpage', 'help', 'hr', 'imagetools', 'insertdatetime', 'legacyoutput', 'media', 'nonbreaking', 'noneditable', 'pagebreak', 'preview', 'print', 'save', 'spellchecker', 'textcolor', 'textpattern', 'toc', 'visualchars','wordcount'];

        foreach($arrPlugins as $plugin) {
            if(file_exists($linkPath.$plugin)) {
                continue;
            }
            $event->addSymlink($srcPath.$plugin, $linkPath.$plugin);
        }

    }
}
